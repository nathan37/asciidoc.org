import puppeteer from 'puppeteer'
import { dirname, join } from 'path'
import { fileURLToPath } from 'url'
import { readdir } from 'fs/promises'

const __dirname = dirname(fileURLToPath(import.meta.url))

function getStyle(file) {
  if (file.startsWith('man-page')) {
    return `.content {
  background-image: url(./img/${file});
  padding-left: 16px;
  padding-top: 16px;
  background-color: #242424;
}

.header {
  height: 67px;
  background-color: #1c1b22;
  border-bottom: 4px solid #2a2932;
}`
  }
  if (file.startsWith('intellij')) {
    return `.frame {
  height: 1280px;
  width: 2048px;
}

.content {
  background-image: url(./img/${file});
  height: 1280px;
  border-left: 3px solid #272727;
  border-right: 3px solid #272727;
  border-bottom: 3px solid #272727;
}

.content::before {
  background: #f3f3f3;
  content: "";
  height: 32px;
  left: 1870px;
  position: absolute;
  top: 60px;
  width: 170px;
}

.content::after {
  background: #fff url(./img/filetype-icon.png) no-repeat;
  background-size: 100% auto;
  content: "";
  height: 26px;
  left: 1707px;
  position: absolute;
  top: 103px;
  width: 22px;
}

.header {
  height: 50px;
  background: linear-gradient(#2a2a2a, #262626);
  border-bottom: 4px solid #272727;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
}

.close {
  display: none;
}`
  }
  return `.content {
  background-image: url(./img/${file});
}`
}

try {
  const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] })
  const page = await browser.newPage()
  await page.goto(`file://${join(__dirname, 'index.html')}`)
  const files = await readdir(join(__dirname, 'img'))
  for (const file of files) {
    await page.addStyleTag({ content: getStyle(file) })
    await page.screenshot({ path: join(__dirname, 'dist', file), fullPage: true, omitBackground: true })
    await page.reload() // reset styles
  }
  await browser.close()
} catch (err) {
  console.error('Something went wrong!', err)
  process.exit(1)
}
